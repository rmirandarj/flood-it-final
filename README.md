Getting started
==============================================================



Setup
-----
```bash
git clone https://rmirandarj@bitbucket.org/rmirandarj/flood-it-final.git
```

Prerequisites
-------------------------

* **Install Java:**

```bash
sudo apt install openjdk-11-jdk
```

Running the application
--------------------
```bash
cd src/main/java
javac Main.java
java Main
```