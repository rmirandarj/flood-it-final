import model.Metrics;
import service.FileService;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {

    private static final int ITERATIONS_BY_INSTANCE = 1;
    private static final FileService fileService = new FileService();

    public static void main(String[] args) throws IOException {
        System.out.println("CPU Core: " + Runtime.getRuntime().availableProcessors());
        System.out.println("CommonPool Parallelism: " + ForkJoinPool.commonPool().getParallelism());
        System.out.println("CommonPool Common Parallelism: " + ForkJoinPool.getCommonPoolParallelism());

        long totalStart = System.currentTimeMillis();
        File[] files = fileService.listInstancesFiles();

        System.out.println("Starting the application to process " + files.length + " instance(s) with " +
                ITERATIONS_BY_INSTANCE + " iteration(s) by instance! \n");

        for (final File file : files) {
            long totalByInstance = System.currentTimeMillis();
            List<Integer> bestSolutions = new ArrayList<>();
            BufferedWriter writer = fileService.getWriter(file);

            List<Integer> results = new ArrayList<>();
            List<Double> times = new ArrayList<>();

            for (int j = 0; j < ITERATIONS_BY_INSTANCE; j++) {
                AntColonyOptimization antColony = new AntColonyOptimization(file.getPath());
                Metrics metricsByIterations = antColony.solve();
                bestSolutions.add(metricsByIterations.getBestSolutionSize());

                results.addAll(metricsByIterations.getSolutions());
                times.addAll(metricsByIterations.getTimesInSeconds());

                writer.append(metricsByIterations.toString()).append("\n\n");
//                System.out.println("Test");
            }

            long totalEndByInstance = System.currentTimeMillis();
            double average = (double) bestSolutions.stream().reduce(Integer::sum).orElse(0) / bestSolutions.size();

            writer.append("\nTime total in seconds: ").append(String.valueOf((totalEndByInstance - totalByInstance) / 1000F));
            writer.append("\nAverage: ").append(Double.toString(average));
            writer.append("\nTimes: ").append(times.toString());
            writer.append("\nResults: ").append(results.toString());
            writer.close();
        }

        System.out.println("Finished with total time in milliseconds: " + (System.currentTimeMillis() - totalStart));
        System.out.println("\nThe results can be found at /resources/results.");
    }
}
