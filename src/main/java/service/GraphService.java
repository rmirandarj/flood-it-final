package service;

import model.Graph;
import model.Node;

import java.util.*;

public class GraphService {

    public void minimizeGraph(Graph graph) {
        Map<Integer, Set<Integer>> redundantNodes = new HashMap<>();

        buildRedundantNodes(graph, redundantNodes);
        addReferencesFromRedundants(graph, redundantNodes);
        removeRedundantsNodes(graph, redundantNodes);
    }

    private void addReferencesFromRedundants(Graph graph, Map<Integer, Set<Integer>> redundantNodes) {
        for (Map.Entry<Integer, Set<Integer>> entry : redundantNodes.entrySet()) {
            Integer nodeMasterIndex = entry.getKey();
            Node nodeMaster = graph.getNodes().get(nodeMasterIndex);
            Set<Integer> children = entry.getValue();

            for (Integer child : children) {
                Node nodeChild = graph.getNodes().get(child);
                nodeChild.getNeighbors().remove(nodeMasterIndex);
                nodeChild.getNeighbors().removeAll(children);

                nodeMaster.getNeighbors().remove(child);
                nodeMaster.addNeighbors(nodeChild.getNeighbors());
                graph.getNodes().remove(child);
            }
        }
    }

    private void removeRedundantsNodes(Graph graph, Map<Integer, Set<Integer>> redundantNodes) {
        for (Map.Entry<Integer, Node> entry : graph.getNodes().entrySet()) {
            Integer nodeIndex = entry.getKey();
            Node node = graph.getNodes().get(nodeIndex);
            Set<Integer> neighborsToRemove = new HashSet<>();
            Set<Integer> neighborsToAdd = new HashSet<>();

            for (Integer neighbor: node.getNeighbors()) {
                if(! graph.getNodes().containsKey(neighbor)){
                    for (Map.Entry<Integer, Set<Integer>> entryy : redundantNodes.entrySet()) {
                        Integer nodeMasterIndex = entryy.getKey();
                        Set<Integer> children = entryy.getValue();

                        if (children.contains(neighbor)) {
                            neighborsToRemove.add(neighbor);
                            neighborsToAdd.add(nodeMasterIndex);
                        }
                    }
                }
            }

            node.getNeighbors().removeAll(neighborsToRemove);
            node.getNeighbors().addAll(neighborsToAdd);
        }
    }

    private void buildRedundantNodes(Graph graph, Map<Integer, Set<Integer>> redundantNodes) {
        Set<Integer> visiteds = new HashSet<>();

        for (Map.Entry<Integer, Node> entry : graph.getNodes().entrySet()) {
            Integer nodeIndex = entry.getKey();
            Node node = graph.getNodes().get(nodeIndex);

            if (!visiteds.contains(nodeIndex)) {
                Set<Integer> neighborsWithSameColor = findNeighbors(graph, nodeIndex, node.getColor());
                visiteds.addAll(neighborsWithSameColor);
                neighborsWithSameColor.remove(nodeIndex);

                if (neighborsWithSameColor.size() > 0)
                    redundantNodes.put(nodeIndex, neighborsWithSameColor);
            }

        }
    }

    private Set<Integer> findNeighbors(Graph graph, Integer initialNode, Integer nodeColor) {
        Set<Integer> neighborsWithSameColor = new HashSet<>();
        Set<Integer> neighbors = new HashSet<>();
        Integer pivotColor = nodeColor;
        Stack<Integer> stack = new Stack<>();

        stack.add(initialNode);
        int stackCounter = 1;

        while (stackCounter > 0) {
            Integer element = stack.pop();
            stackCounter--;
            for (Integer neighbor : graph.getNodes().get(element).getNeighbors()) {
                if (pivotColor.compareTo(graph.getNodes().get(neighbor).getColor()) == 0 &&
                        ! neighbors.contains(neighbor) && ! neighborsWithSameColor.contains(neighbor)) {
                    stack.add(neighbor);
                    stackCounter++;
                    neighborsWithSameColor.add(neighbor);
                } else if (!neighbors.contains(neighbor))
                    neighbors.add(neighbor);
            }
        }
        return neighborsWithSameColor;
    }

}
