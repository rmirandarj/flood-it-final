package service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class FileService {

    private static final String RELATIVE_PATH = "resources";
    private static final String INSTANCES_PATH = RELATIVE_PATH + "/instances";
    private static final String RESULTS_PATH = RELATIVE_PATH + "/results";

    public File instancesPath;
    public File resultsPath;

    public FileService() {
        instancesPath = new File(INSTANCES_PATH);
        resultsPath = new File(RESULTS_PATH);
    }

    public File[] listInstancesFiles(){
        return Objects.requireNonNull(instancesPath.listFiles());
    }

    public BufferedWriter getWriter(File fileEntry) throws IOException {
        return new BufferedWriter(new FileWriter(getTargetResult(fileEntry)));
    }

    public List<Integer> readDataAsIntegers(String filepath) throws IOException {
        String data = this.readFile(filepath);
        data = data.replaceAll("\n", "");
        String[] stringArray = data.split(" ");

        return Stream.of(stringArray).map(Integer::parseInt).collect(toList());
    }

    private File getTargetResult(File fileEntry){
        return new File(RESULTS_PATH + "/" + fileEntry.getName());
    }

    private String readFile(String filePath) throws IOException {
        Path path = Paths.get(filePath);
        Stream<String> lines = Files.lines(path);
        String data = lines.collect(Collectors.joining("\n"));
        lines.close();
        return data;
    }
}
