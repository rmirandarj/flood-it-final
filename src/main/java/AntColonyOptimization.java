import model.*;
import service.FileService;
import service.GraphService;

import java.io.IOException;
import java.util.*;
import java.util.stream.IntStream;

import static java.lang.Math.pow;
import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;

public class AntColonyOptimization implements IAlgorithm{
    private static final double ALPHA = 1;
    private static final double BETA = 5;
    private static final double EVAPORATION = 0.5;
    private static final double Q = 500;
    private static final double ANT_FACTOR = 0.15;
    private static final double RANDOM_FACTOR = 0.08;
    private static final int MAX_INITIAL_MOVEMENTS = 10;

    private List<Trail> trails = new ArrayList<>();
    private List<Ant> ants = new ArrayList<>();
    private Random random = new Random();

    private List<Integer> bestSolution = new ArrayList<>();

    private Graph graph;
    private int numberOfColors;

    private List<Integer> colors = new ArrayList<>();

    private GraphService graphService = new GraphService();

    public AntColonyOptimization(String filepath) throws IOException {
        List<Integer> dataArray = new FileService().readDataAsIntegers(filepath);
        int row = dataArray.remove(0);
        int col = dataArray.remove(0);
        numberOfColors = dataArray.remove(0);
        int numberOfAnts = (int) (row * col * ANT_FACTOR);
        graph = GraphFactory.getGraph(row, col, dataArray);

        graphService.minimizeGraph(graph);

        IntStream.range(0, numberOfColors).forEach(i -> colors.add(i));

        for (int i = 0; i < numberOfAnts; i++) {
            ants.add(new Ant(graph));
        }
    }

    /**
     * Generate initial solution
     */
    public void setupAnts() {
        for (Ant ant : ants) {
            List<Double> pheromones = initialSolution(ant.getGraph(), ant.getColorsTrail());
            if (!(isSolutionEqualsAnyTrail(ant.getColorsTrail()) > 0))
                trails.add(new Trail(ant.getColorsTrail(), pheromones));
        }
    }

    private int isSolutionEqualsAnyTrail(List<Integer> colorsTrail) {
        if (colorsTrail.size() == 0)
            return 0;

        int trailsSize = trails.size();
        int colorsTrailSize = colorsTrail.size();

        for (int i = 0; i < trailsSize; i++){
            int found = 1;

            if (colorsTrailSize != trails.get(i).getPath().size()) {
                continue;
            }

            for (int j = 0; j < colorsTrailSize; j++) {
                if (j < trails.get(i).getPath().size() && !colorsTrail.get(j).equals(trails.get(i).getPath().get(j))) {
                    found = 0;
                    break;
                }
            }
            if (found > 0) return i;
        }
        return 0;
    }

    private boolean isSolutionOnTheSamePath(List<Integer> colorsTrail, Trail trail) {
        if (colorsTrail.size() == 0)
            return true;
        if(colorsTrail.size() > trail.getPath().size())
            return false;

        int bound = colorsTrail.size();
        for (int i = 0; i < bound; i++) {
            if (!colorsTrail.get(i).equals(trail.getPath().get(i))) {
                return false;
            }
        }
        return true;
    }

    private List<Double> initialSolution(Graph graph, List<Integer> colorsTrail) {
        List<Double> pheromones = new ArrayList<>();
        double initialPheromone = 1.0;
        int max = graph.getNodes().size() * MAX_INITIAL_MOVEMENTS;

        for (int movements = 0; movements < max && ! isGraphFlooded(graph); movements++){
            List<Integer> nodesWillFlood = new ArrayList<>();
            Set<Integer> neighbors = new HashSet<>();
            List<Integer> candidateColors = new ArrayList<>();

            findNeighborsAndCandidateColors(graph, nodesWillFlood, neighbors, candidateColors);

            Integer sortedColor = getRandomColor(candidateColors);

            colorsTrail.add(sortedColor);
            pheromones.add(initialPheromone);

            fillGraph(graph, nodesWillFlood, sortedColor);
        }
        return pheromones;
    }

    private Integer getRandomColor(List<Integer> candidateColors){
        int rndIndex = new Random().nextInt(candidateColors.size());
        return candidateColors.get(rndIndex);
    }

    private void findNeighborsAndCandidateColors(Graph graph, List<Integer> nodesWillFlood, Set<Integer> neighbors, List<Integer> candidateColors) {
        Integer pivotColor = graph.getNodes().get(0).getColor();
        Stack<Integer> stack = new Stack<>();
        stack.add(0);
        int stackCounter = 1;
        nodesWillFlood.add(0);

        while (stackCounter > 0) {
            Integer element = stack.pop();
            stackCounter--;
            for (Integer neighbor : graph.getNodes().get(element).getNeighbors()) {
                if (pivotColor.compareTo(graph.getNodes().get(neighbor).getColor()) == 0 && !neighbors.contains(neighbor)) {
                    stack.add(neighbor);
                    stackCounter++;
                    nodesWillFlood.add(neighbor);
                } else if (!neighbors.contains(neighbor))
                    if (!pivotColor.equals(graph.getNodes().get(neighbor).getColor()))
                        candidateColors.add(graph.getNodes().get(neighbor).getColor());
                neighbors.add(neighbor);
            }
        }
    }

    private boolean isGraphFlooded(Graph graph) {
        Integer pivotColor = graph.getNodes().get(0).getColor();

        for (Map.Entry<Integer, Node> entry : graph.getNodes().entrySet()) {
            Node v = entry.getValue();
            if (pivotColor.compareTo(v.getColor()) != 0) {
                return false;
            }
        }
        return true;
    }

    private void updateTrails() {
        evaporatePheromones();
        updatePheromones();
    }

    private void updatePheromones() {
        for (Ant ant : ants) {
            int trailIndex = isSolutionEqualsAnyTrail(ant.getColorsTrail());
            if (trailIndex > 0) {
                updatePheromonesInTrail(ant.getColorsTrail(), trailIndex);
            } else {
                addNewTrail(ant.getColorsTrail());
            }
        }
    }

    private void updatePheromonesInTrail(List<Integer> solutions, int trailIndex) {
        List<Double> updatedPheromones = trails.get(trailIndex).getPheromones().stream().map(ph -> ph + (Q / solutions.size())).collect(toList());
        trails.get(trailIndex).setPheromones(updatedPheromones);
    }

    private void addNewTrail(List<Integer> solutions) {
        List<Double> pheromones = new ArrayList<>();
        int size = solutions.size();

        for (int i = 0; i < size; i++){
            double contribution = Q / solutions.size();
            pheromones.add(contribution);
        }
        trails.add(new Trail(solutions, pheromones));
    }

    private void evaporatePheromones() {
        for (Trail trail : trails) {
            trail.setPheromones(trail.getPheromones().stream().map(ph -> ph * EVAPORATION).collect(toList()));
        }
    }

    /**
     * At each iteration, move ants
     */
    private void moveAnts() {
        int maxTrailLength = getMaxTrailLength();

        for(int trailIndex = 0; trailIndex < maxTrailLength; trailIndex++){
            final int newTrailIndex = trailIndex;
            ants.parallelStream().forEach(ant -> {
                List<Integer> neighbors = new ArrayList<>();
                if (!isGraphFlooded(ant.getGraph())) {
                    Integer color = selectNextColor(ant, neighbors, newTrailIndex);
                    ant.addSolution(color);
                    ant.fillGraph(neighbors, color);
                }
            });
        }
    }

    private Integer selectNextColor(Ant ant, List<Integer> neighbors, int trailIndex) {
        Set<Integer> visitors = new HashSet<>();
        List<Integer> candidateColors = new ArrayList<>();
        double[] probabilities = new double[numberOfColors];

        findNeighborsAndCandidateColors(ant.getGraph(), neighbors, visitors, candidateColors);

        if (random.nextDouble() < RANDOM_FACTOR)
            return getRandomColor(candidateColors);

        calculateProbabilities(ant, candidateColors, trailIndex, probabilities);

        double r = random.nextDouble();
        double total = 0.0;

        for(int j = 0; j < numberOfColors; j++) {
            total = total + probabilities[j];
            if (total >= r)
                return j;
        }

        return getRandomColor(candidateColors);
    }

    private void calculateProbabilities(Ant ant, List<Integer> candidateColors, int trailIndex, double[] probabilities) {
        double denominator = getTotalPheromone(ant.getColorsTrail(), trailIndex);

        if(denominator == 0)
            zeroProbabilitiesArray(probabilities);
        else{
            for (Integer color : colors) {
                if(candidateColors.contains(color)){
                    double numerator = getTotalPheromoneByColor(ant.getColorsTrail(), trailIndex, color);
                    probabilities[color] = numerator / denominator;
                }
                else {
                    probabilities[color] = 0d;
                }
            }
        }

    }

    private double getTotalPheromoneByColor(List<Integer> colorsTrail, int trailIndex, int color) {
        double totalPheromone = 0d;

        for (Trail trail : trails) {
            int movements = trail.getPath().size();
            if(trailIndex < movements && isSolutionOnTheSamePath(colorsTrail, trail) && trail.getPath().get(trailIndex).equals(color)){
                double leftPheromone = trail.getPheromones().get(trailIndex);
                totalPheromone += pow(leftPheromone, ALPHA) * pow(1.0 / movements, BETA);
            }
        }

        return totalPheromone;
    }

    private void zeroProbabilitiesArray(double[] probabilities) {
        int bound = numberOfColors;
        for (int i = 0; i < bound; i++) {
            probabilities[i] = 0.0;
        }
    }

    private double getTotalPheromone(List<Integer> colorsTrail, int trailIndex) {
        double totalPheromone = 0d;

        for (Trail trail: trails) {
            int movements = trail.getPath().size();

            if(trailIndex < movements && isSolutionOnTheSamePath(colorsTrail, trail)){
                double leftPheromone = trail.getPheromones().get(trailIndex);
                totalPheromone += pow(leftPheromone, ALPHA) * pow(1.0 / movements, BETA);
            }
        }
        return totalPheromone;
    }

    private int getMaxTrailLength() {
        return trails.stream().max(Comparator.comparing(trail -> trail.getPath().size())).orElseThrow(NoSuchElementException::new).getPath().size();
    }

    /**
     * Update the best solution
     */
    private boolean updateBest() {
        if (bestSolution.size() == 0) {
            bestSolution = ants.get(0).getColorsTrail();
        }

        int bestSolSize = bestSolution.size();
        for (Ant a : ants) {
            if (a.getColorsTrail().size() > 0 && isGraphFlooded(a.getGraph()) && a.getColorsTrailSize() < bestSolSize) {
                bestSolution = new ArrayList<>(a.getColorsTrail());
                return true;
            }
        }

//        System.out.println("Best: " + bestSolution.size() + " --------- Trails size: " + trails.size());
        return false;
    }

    /**
     * Clear trails after simulation
     */
    private void clearAnts() {
        ants.parallelStream().forEach(ant -> ant.clear(graph));
    }

    @Override
    public Metrics solve() {
        long start = System.currentTimeMillis();
        int i = 0;
        int maxWithoutConvergence = 0;
        long boundInSeconds = (long) (graph.getNodes().size() / 10d * 1000);
        long limitTime = start + boundInSeconds;
        List<Integer> bestSolutions = new ArrayList<>();
        List<Double> timesInSeconds = new ArrayList<>();

        setupAnts();
        updateBest();
        clearAnts();

        long start2 = System.currentTimeMillis();

        int count = 0;
        while (System.currentTimeMillis() < limitTime) {
            moveAnts();
            updateTrails();
            if (updateBest()){
                if(i > maxWithoutConvergence) maxWithoutConvergence = i;
                i = 0;

                bestSolutions.add(bestSolution.size());
                timesInSeconds.add( (double) ((System.currentTimeMillis() - start2) / 1000F) );

                start2 = System.currentTimeMillis();
            }
            else {
                i++;
            }
            clearAnts();
            removeOldTrails();

//            count++;
        }

        long end = System.currentTimeMillis();

//        System.out.println("Total iterations: " + count);
        return new Metrics(maxWithoutConvergence, (end - start) / 1000F, bestSolution.size(), bestSolutions, timesInSeconds);
    }

    private void removeOldTrails() {
        trails = trails.parallelStream().filter(t -> t.getPheromones().get(0) >= 5.0).collect(toList());
    }

    private void printGraphBySolution(){
        List<Integer> solution = asList(2, 3, 1, 4, 0, 1, 0, 1, 2, 0, 4, 7, 1, 2, 1, 0, 3, 1, 0, 4, 2, 7, 3, 5, 0, 1, 2, 7, 4, 6);

        solution.forEach(sol -> {
            List<Integer> nodesWillFlood = new ArrayList<>();
            Set<Integer> neighbors = new HashSet<>();
            List<Integer> candidateColors = new ArrayList<>();

            findNeighborsAndCandidateColors(graph, nodesWillFlood, neighbors, candidateColors);

            fillGraph(graph, nodesWillFlood, sol);
        });

        System.out.println(isGraphFlooded(graph));
    }

    private void fillGraph(Graph graph, List<Integer> neighbors, Integer sortedColor) {
        for (Integer n : neighbors) {
            graph.getNodes().get(n).setColor(sortedColor);
        }
    }
}
