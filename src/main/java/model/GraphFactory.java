package model;

import java.util.List;

public class GraphFactory {

    public static Graph getGraph(int row, int col, List<Integer> colors){
        Graph graph = new Graph();
        int len = row * col;
        int currentNode = 0;

        for (int i = 0; i < len; i++){
            Node node = new Node();
            node.setColor(colors.get(i));

            if (i == 0) {
                node.addNeighbor(i + 1);
                node.addNeighbor(i + col);
            }
            else if (i == col - 1) {
                node.addNeighbor(i - 1);
                node.addNeighbor(i + col);
            }
            else if (i == (col * row - col)){
                node.addNeighbor(i + 1);
                node.addNeighbor(i - col);
            }
            else if (i == (col * row - 1)){
                node.addNeighbor(i - 1);
                node.addNeighbor(i - col);
            }
            else if (i < col) {  //Primeira linha
                node.addNeighbor(i + 1);
                node.addNeighbor(i - 1);
                node.addNeighbor(i + col);
            }
            else if (i % col == 0) {  //Primeira coluna
                node.addNeighbor(i + 1);
                node.addNeighbor(i + col);
                node.addNeighbor(i - col);
            }
            else if (i % col == col - 1) {  //Ultima coluna
                node.addNeighbor(i - 1);
                node.addNeighbor(i + col);
                node.addNeighbor(i - col);
            }
            else if (i > (col * row - col)) {  //Ultima linha
                node.addNeighbor(i + 1);
                node.addNeighbor(i - 1);
                node.addNeighbor(i - col);
            }
            else {
                node.addNeighbor(i + 1);
                node.addNeighbor(i - 1);
                node.addNeighbor(i + col);
                node.addNeighbor(i - col);
            }
            graph.addNode(currentNode, node);
            currentNode += 1;
        }
        return graph;
    }
}
