package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Ant {
    private int colorsTrailSize;
    private List<Integer> colorsTrail = new ArrayList<>();
    private Graph graph;

    public Ant(Graph graph){
        this.graph = new Graph(graph.getNodes());
    }

    public void addSolution(Integer color) {
        this.colorsTrail.add(color);
        this.colorsTrailSize++;
    }

    public void clear(Graph graph) {
        this.colorsTrailSize = 0;
        this.colorsTrail = new ArrayList<>();
        this.graph = new Graph(graph.getNodes());
    }

    public void fillGraph(List<Integer> neighbors, Integer sortedColor) {
        for (Integer n : neighbors) {
            graph.getNodes().get(n).setColor(sortedColor);
        }
    }
    public int getColorsTrailSize() {
        return colorsTrailSize;
    }

    public List<Integer> getColorsTrail() {
        return colorsTrail;
    }

    public Graph getGraph() {
        return graph;
    }

    private boolean isGraphFlooded() {
        Integer pivotColor = graph.getNodes().get(0).getColor();

        for (Map.Entry<Integer, Node> entry : graph.getNodes().entrySet()) {
            Node v = entry.getValue();
            if (pivotColor.compareTo(v.getColor()) != 0) {
                return false;
            }
        }
        return true;
    }
}