package model;

import java.util.Arrays;
import java.util.List;

public class Metrics {
    private int iterationsWithoutConvergences;
    private double totalTime;
    private int bestSolutionSize;
    private List<Integer> solutions;
    private List<Double> timesInSeconds;

    public Metrics(int iterationsWithoutConvergences, double totalTime, int bestSolutionSize, List<Integer> solutions, List<Double> timesInSeconds) {
        this.iterationsWithoutConvergences = iterationsWithoutConvergences;
        this.totalTime = totalTime;
        this.bestSolutionSize = bestSolutionSize;
        this.solutions = solutions;
        this.timesInSeconds = timesInSeconds;
    }

    public int getIterationsWithoutConvergences() {
        return iterationsWithoutConvergences;
    }

    public void setIterationsWithoutConvergences(int iterationsWithoutConvergences) {
        this.iterationsWithoutConvergences = iterationsWithoutConvergences;
    }

    public double getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(double totalTime) {
        this.totalTime = totalTime;
    }

    public int getBestSolutionSize() {
        return bestSolutionSize;
    }

    public void setBestSolutionSize(int bestSolutionSize) {
        this.bestSolutionSize = bestSolutionSize;
    }

    public List<Integer> getSolutions() {
        return solutions;
    }

    public void setSolutions(List<Integer> solutions) {
        this.solutions = solutions;
    }

    public List<Double> getTimesInSeconds() {
        return timesInSeconds;
    }

    public void setTimesInSeconds(List<Double> timesInSeconds) {
        this.timesInSeconds = timesInSeconds;
    }

    @Override
    public String toString() {
        return "Metrics{" +
                "iterationsWithoutConvergences=" + iterationsWithoutConvergences +
                ", totalTime=" + totalTime +
                ", bestSolutionSize=" + bestSolutionSize +
                ", solutions=" + solutions +
                ", timesInSeconds=" + timesInSeconds +
                '}';
    }
}
