package model;

import java.util.List;

public class Trail {
    private List<Integer> path;
    private List<Double> pheromones;

    public Trail(List<Integer> solutions, List<Double> pheromones) {
        this.path = solutions;
        this.pheromones = pheromones;
    }

    public List<Integer> getPath() {
        return path;
    }

    public void setPath(List<Integer> path) {
        this.path = path;
    }

    public List<Double> getPheromones() {
        return pheromones;
    }

    public void setPheromones(List<Double> pheromones) {
        this.pheromones = pheromones;
    }
}
