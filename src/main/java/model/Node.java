package model;

import java.util.*;

public class Node {
    private Integer color;
    private Set<Integer> neighbors = new HashSet<>();

    public Node(){
    }

    public Node(Node node) {
        this.color = node.getColor();
        this.neighbors = new HashSet<>(node.neighbors);
    }

    public void addNeighbor(Integer neighbor){
        this.neighbors.add(neighbor);
    }

    public void addNeighbors(Collection<Integer> neighbors){
        this.neighbors.addAll(neighbors);
    }

    public Integer getColor() {
        return color;
    }

    public void setColor(Integer color) {
        this.color = color;
    }

    public Set<Integer> getNeighbors() {
        return neighbors;
    }

    public void setNeighbors(Set<Integer> neighbors) {
        this.neighbors = neighbors;
    }
}
