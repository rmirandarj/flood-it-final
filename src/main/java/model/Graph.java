package model;

import java.util.HashMap;
import java.util.Map;

public class Graph {
    private Map<Integer, Node> nodes = new HashMap<>();

    public Graph(){
    }

    public Graph(Map<Integer, Node> nodes){
        Map<Integer, Node> newNodes = new HashMap<>();
        for (Map.Entry<Integer, Node> entry : nodes.entrySet()) {
            newNodes.put(entry.getKey(), new Node(entry.getValue()));
        }
        this.nodes = newNodes;
    }

    public void addNode(Integer i, Node node){
        this.nodes.put(i, node);
    }

    public boolean nodesHaveEqualColor(Integer nodeColor, Integer neighborIndex){
        return nodeColor.equals(this.getNodes().get(neighborIndex).getColor());
    }

    public Map<Integer, Node> getNodes() {
        return nodes;
    }

    public void setNodes(Map<Integer, Node> nodes) {
        this.nodes = nodes;
    }
}

