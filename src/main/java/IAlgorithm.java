import model.Metrics;

public interface IAlgorithm {
    public Metrics solve();
}
